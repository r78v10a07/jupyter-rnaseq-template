import os
import re
import json
import pandas
import math
import pickle
import zipfile
import distutils.spawn
import numpy as np
import scipy.stats as stats
import seaborn as sns

from IPython.display import display

import matplotlib
import matplotlib.pyplot as plt

from IPython.display import HTML
from IPython.display import display, Markdown, Latex

###############################################################
#
#    Update cutoff values
#
###############################################################

# log2(FoldChange)
fc = 2.0

# max FDR (adjusted P-Value)
fdr = 0.05

###############################################################
#
#    Project absolute path
#
###############################################################

WORKDIR = os.path.dirname(os.path.abspath(os.path.dirname(__file__)))

###############################################################
#
#    Update genome files and indexes path
#
# If indexes and reference bed files does not exist can be created using 
# the notebooks but you need to have writing permission in the GENOME dir
#
###############################################################
GENOME = '/gfs/data/genomes/igenomes/Homo_sapiens/UCSC/hg38'
GENOME_NAME = 'hg38'
STAR_INDEX = os.path.join(GENOME, 'STAR')
GENOME_FASTA = os.path.join(GENOME, 'genome.fa')
GENOME_GTF = os.path.join(GENOME, 'genes.gtf')
GENOME_BED = os.path.join(GENOME, 'genes.bed')

###############################################################
#
#    Dataset (experiment) to analyze
#
# The path is $WORKDIR/data/$DATASET
#
# To use multiple datasets (experiments) this variable should be overwritten
# in the notebooks
#
###############################################################

DATASET = 'PRJNA339968'

###############################################################
#
#    Docker configuration
#
###############################################################

DOCKER = True

###############################################################
#
#    cwl-runner with absolute path if necesary 
#
###############################################################

CWLRUNNER_TOOL = 'cwl-runner'
CWLRUNNER_TOOL_PATH = distutils.spawn.find_executable(CWLRUNNER_TOOL)
if not CWLRUNNER_TOOL_PATH:
    print('WARNING: %s not in path' % (CWLRUNNER_TOOL))
    print('Install:')
    print('pip install cwltool')
    print('pip install cwl-runner')
else:
    CWLRUNNER = CWLRUNNER_TOOL_PATH
if not DOCKER:
    CWLRUNNER = CWLRUNNER + ' --no-container '

###############################################################

CONFIG = os.path.join(WORKDIR,'config')
if not os.path.exists(CONFIG):
    os.mkdir(CONFIG)

DATA = os.path.join(WORKDIR,'data')
if not os.path.exists(DATA):
    os.mkdir(DATA)
    
BIN = os.path.join(WORKDIR,'bin')
if not os.path.exists(BIN):
    os.mkdir(BIN)
    
RESULTS = os.path.join(WORKDIR,'results')
if not os.path.exists(RESULTS):
    os.mkdir(RESULTS)
    
NOTEBOOKS = os.path.join(WORKDIR,'notebooks')
if not os.path.exists(NOTEBOOKS):
    os.mkdir(NOTEBOOKS)
    
SRC = os.path.join(WORKDIR,'src')
if not os.path.exists(SRC):
    os.mkdir(SRC)
    
TMP = os.path.join(WORKDIR,'tmp')
if not os.path.exists(TMP):
    os.mkdir(TMP)
    
CWLURL = 'https://gitlab.com/r78v10a07/cwl-workflow/raw/master'    
CWLTOOLS = os.path.join(CWLURL, 'tools')
CWLWORKFLOWS = os.path.join(CWLURL, 'workflows')

CWLRUNNER = CWLRUNNER + ' --tmp-outdir-prefix=' + TMP + '/ --tmpdir-prefix=' + TMP + '/ '

